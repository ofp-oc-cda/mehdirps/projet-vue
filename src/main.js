import {
    createApp
} from 'vue';

import {
    createPinia
} from 'pinia';


import App from './App.vue';

import './assets/css/reset.css';
import './assets/css/styles.css';

import {
    createRouter,
    createWebHashHistory
} from 'vue-router';
import Home from './pages/Home.vue';
import Restaurant from './pages/Restaurant.vue';
import AllUsers from './pages/AllUsers.vue';
import User from './pages/User.vue'
import Login from './pages/Login.vue'
import Post from './pages/Post.vue'

const routes = [{
        name: 'Home',
        path: '/',
        component: Home
    },
    {
        name: 'Restaurant',
        path: '/restaurant/:name',
        component: Restaurant
    },
    {
        name: 'AllUsers',
        path: '/users',
        component: AllUsers
    },
    {
        name: 'Users',
        path: '/users/:id',
        component: User
    },
    {
        name: 'Login',
        path: '/login',
        component: Login
    },
    {
        name: 'Post',
        path: '/post/:id',
        component: Post
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

const vueApp = createApp(App);

vueApp.use(router);
vueApp.use(createPinia());

vueApp.mount('#app');