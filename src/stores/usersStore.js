import {
    defineStore
} from "pinia";

import axios from "axios";

export const useUsersStore = defineStore('Users', {
    state: () => {
        return {
            users: {},
        }
    },

    actions: {
        fetchUsers() {

            const token = sessionStorage.token;

            if (this.users.length > 0) {
                return
            }
            return axios
                .get("https://freefakeapi.io/authapi/users", {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then(response => {
                    const tmp = {};
                    response.data.forEach((user) => tmp[user.id] = user)
                    this.users = tmp;
                });
        },
    },
    getters: {
        getUsers(state) {
            return Object.values(state.users)
        },
        getUser(state) {
            return (id) => {
                return state.users[id];
            }
        }
    }
});