import {
    defineStore
} from "pinia";

import axios from "axios";

export const usePostsStore = defineStore('Posts', {
    state: () => {
        return {
            posts: {},
        }
    },

    actions: {
        fetchPosts() {

            const token = sessionStorage.token;

            if (this.posts.length > 0) {
                return
            }
            return axios
                .get("https://freefakeapi.io/authapi/posts", {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                })
                .then(response => {
                    const tmp = {};
                    response.data.forEach((post) => tmp[post.id] = post);
                    this.posts = tmp;
                });
        },
    },
    getters: {
        getPosts(state) {
            return Object.values(state.posts);
        },
        getPost(state) {
            return (id) => {
                return state.posts[id];
            }
        }
    }
});